structure PPrint:
          sig
              val asString: Absyn.exp -> string
              val print: TextIO.outstream * Absyn.exp -> unit
          end =
struct

structure A = Absyn
structure S = Symbol

val i2s = Int.toString

fun len (s : S.symbol) : int = (String.size o S.name) s

fun asString (e : A.exp) : string =
  let fun indent 0 = ""
        | indent i = indent (i - 1) ^ " "

      fun op2s A.PlusOp = "+"
        | op2s A.MinusOp = "-"
        | op2s A.TimesOp = "*"
        | op2s A.DivideOp = "/"
        | op2s A.EqOp = "="
        | op2s A.NeqOp = "<>"
        | op2s A.LtOp = "<"
        | op2s A.LeOp = "<="
        | op2s A.GtOp = ">"
        | op2s A.GeOp = ">="
        | op2s A.ExponentOp = "^"

      fun e2s' lvl e =
        let val newline = "\n" ^ indent lvl

            fun e2s (A.VarExp var) = v2s var
              | e2s (A.NilExp) = "nil"
              | e2s (A.IntExp i) = i2s i
              (* TODO: handle special chars in StringExp *)
              | e2s (A.StringExp (s, _)) = "\"" ^ s ^ "\""
              | e2s (A.CallExp {func, args, pos}) =
                let val arg_lvl = lvl + (len func) + 1
                    fun doargs [] = ""
                      | doargs [(arg, _)] = e2s' arg_lvl arg
                      | doargs ((arg, _) :: args) = e2s' arg_lvl arg ^ ", " ^
                                               doargs args
                in S.name func ^ "(" ^ doargs args ^ ")"
                end
              | e2s (A.OpExp {left, oper, right, pos}) =
                e2s left ^ " " ^ op2s oper ^ " " ^ e2s right
              | e2s (A.RecordExp {fields, typ, pos}) =
                let val field_lvl = lvl + (len typ) + 2
                    val nl_field = "\n" ^ indent field_lvl
                    fun dofields [] = ""
                      | dofields [(f, e, _)] =
                        S.name f ^ " = " ^ e2s' (field_lvl + (len f) + 3) e
                      | dofields ((f, e, _) :: fs) =
                        S.name f ^ " = " ^ e2s' (field_lvl + (len f) + 3) e ^
                        "," ^ nl_field ^ dofields fs
                in S.name typ ^ " {" ^ dofields fields ^ "}"
                end
              | e2s (A.SeqExp es) =
                let fun seq_aux [] = ""
                      | seq_aux [(e, _)] = e2s' (lvl + 1) e
                      | seq_aux ((e, _) :: es) = e2s' (lvl + 1) e ^ ";"
                                                 ^ newline ^ " " ^
                                                 seq_aux es
                in "(" ^ seq_aux es ^ ")"
                end
              | e2s (A.AssignExp {var, exp, pos}) =
                v2s var ^ " := " ^ e2s exp
              | e2s (A.IfExp {test, thn, els, pos}) =
                "if " ^ e2s' (lvl + 3) test ^ newline ^
                "then " ^ e2s' (lvl + 5) thn ^
                (case els
                  of SOME els' => newline ^
                                  "else " ^ e2s' (lvl + 5) els'
                   | NONE => "")
              | e2s (A.WhileExp {test, body, pos}) =
                "while " ^ e2s' (lvl + 6) test ^ newline ^
                e2s body
              | e2s (A.ForExp {var, escape, lo, hi, body, pos}) =
                let val lo_lvl = lvl + 4 + (len var) + 4
                    val hi_lvl = lo_lvl + 3 + 4 (* +3 is for lo *)
                in "for " ^ S.name var ^ " := " ^ e2s' lo_lvl lo ^ " to " ^
                   e2s' hi_lvl hi ^ newline ^
                   "do " ^ e2s' (lvl + 3) body
                end
              | e2s (A.BreakExp _) = "break"
              | e2s (A.LetExp {decls, body, pos}) =
                let val dec_lvl = lvl + 4
                    val nl_dec = "\n" ^ indent dec_lvl
                    fun dodecs [] = ""
                      | dodecs [dec] = d2s dec_lvl dec
                      | dodecs (dec :: decs) = d2s dec_lvl dec ^ nl_dec ^
                                               dodecs decs
                in (case decls
                     of [] => "let"
                      | _ => "let " ^ dodecs decls) ^ newline ^
                   "in " ^ e2s_strip (lvl + 3) body ^ newline ^
                   "end"
                end
              | e2s (A.ArrayExp {typ, size, init, pos}) =
                let val size_lvl = lvl + (len typ) + 2
                    val init_lvl = size_lvl + 3 + 4 (* +3 is for size *)
                in S.name typ ^ " [" ^ e2s' size_lvl size ^ "] of " ^
                   e2s' init_lvl init
                end
            and v2s (A.SimpleVar (v, _)) = S.name v
              | v2s (A.FieldVar (var, f, _)) =
                v2s var ^ "." ^ S.name f
              | v2s (A.SubscriptVar (var, exp, _)) =
                v2s var ^ "[" ^ e2s exp ^ "]"
            and d2s dlvl (A.FunctionDec fundecs) =
                let val nl = "\n" ^ indent dlvl
                    val body_indent = 4
                    fun doparam {name, escape, typ = (typ', _), pos} =
                      S.name name ^ " : " ^ S.name typ'
                    fun doparams [] = ""
                      | doparams [p] = doparam p
                      | doparams (p :: ps) = doparam p ^ ", " ^
                                             doparams ps
                    fun dofun {name, params, result, body, pos} =
                        "function " ^ S.name name ^ " (" ^ doparams params ^ ") " ^
                        (case result
                          of NONE => ""
                          | SOME (s, _) => ": " ^ S.name s ^ " ") ^
                        "=" ^ nl ^ (indent body_indent) ^ e2s' (dlvl + body_indent)
                                                               body
                    fun dofuns [] = ""
                      | dofuns [f] = dofun f
                      | dofuns (f::fs) = dofun f ^ nl ^
                                         dofuns fs
                in dofuns fundecs
                end
              | d2s dlvl (A.VarDec {name, escape, typ, init, pos}) =
                let val init_lvl = dlvl + 4 + (len name) + 1 +
                                   (case typ
                                     of NONE => 0
                                      | SOME (typ', _) => 2 + (len typ') + 1) +
                                   3
                in "var " ^ S.name name ^ " " ^ (case typ
                                                  of NONE => ""
                                                   | SOME (typ', _) =>
                                                     ": " ^ S.name typ' ^ " ") ^
                   ":= " ^ e2s' init_lvl init
                end
              | d2s dlvl (A.TypeDec tydecs) =
                let val nl = "\n" ^ indent dlvl
                    fun dotyp [] = ""
                      | dotyp [{name, ty, pos}] =
                        "type " ^ S.name name ^ " = " ^ ty2s ty
                      | dotyp ({name, ty, pos} :: tys) =
                        "type " ^ S.name name ^ " = " ^ ty2s ty ^ nl ^
                        dotyp tys
                in dotyp tydecs
                end
            and ty2s (A.NameTy (name, _)) = S.name name
              | ty2s (A.RecordTy fields) =
                let fun dofs [] = ""
                      | dofs [{name, escape, typ = (t, _), pos}] =
                        S.name name ^ " : " ^ S.name t
                      | dofs ({name, escape, typ = (t, _), pos} :: fs) =
                        S.name name ^ " : " ^ S.name t ^ ", " ^
                        dofs fs
                in "{" ^ dofs fields ^ "}"
                end
              | ty2s (A.ArrayTy (s, _)) = "array of " ^ S.name s
        in e2s e
        end
      and e2s_strip lvl (A.SeqExp es) =
          let val newline = "\n" ^ indent lvl
              fun seq_aux [] = ""
                | seq_aux [(e, _)] = e2s' lvl e
                | seq_aux ((e, _) :: es) = e2s' lvl e ^ ";" ^ newline ^
                                           seq_aux es
          in seq_aux es
          end
        | e2s_strip lvl e = e2s' lvl e

  in e2s' 0 e ^ "\n"
  end

fun print (outstream, e0) =
    ( TextIO.output (outstream, asString e0)
    ; TextIO.flushOut outstream)

end (* PPrint *)
