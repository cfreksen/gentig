(* Aarhus University, Compilation 2016  *)
(* DO NOT DISTRIBUTE                    *)
(* DO NOT CHANGE THIS FILE              *)


structure Main =
struct

fun withOpenFile fname f =
  let val out = TextIO.openOut fname
   in (f out before TextIO.closeOut out)
  end

fun compile arg =
    let
        val absyn = GenTig.generate ()
        val absyn_s = PrintAbsyn.asString absyn
        val tiger_s = PPrint.asString absyn
        val _ = print tiger_s
    in case arg
        of SOME outpath =>
           let
               val outfile = TextIO.openOut outpath
               val _ = TextIO.output (outfile, tiger_s)
               val _ = TextIO.closeOut outfile
           in ()
           end
         | NONE => ()
    end



fun exportedFn (self, [outpath]) = (compile (SOME outpath); 0)
  | exportedFn (self, []) = (compile (NONE); 0)

  | exportedFn (self, _) = (print "Expects arguments <outfile>"; ~1)

end (* Main *)
