SMLOPTS=-Ccontrol.poly-eq-warn=false -m '$$smlnj-tdp/back-trace.cm'

std: clean standalone run


# set up SML for interactive use

interact:
	rlwrap sml $(SMLOPTS) sources.cm

# create a standalone executable via gentig.x86-linux

standalone:
	ml-build sources.cm Main.exportedFn gentig

gentig.x86-linux: standalone

# abbreviations

ia: interact

sa: standalone

rs: rescue

install: standalone

run:
	sml @SMLload=gentig.x86-linux generated.tig

# prepare for emacs multi-file search based on a 'TAGS' file

TAGS:
	etags *.sml

# reminder about how to get help

smlopts:
	sml -help | less

smlvars:
	sml -E | less

# remove generated files, enforce full recompile/regenerate action

clean:
	rm -rf .cm
	rm -f *.x86-linux
	rm -f *.x86-darwin
