(* gentig -- Generate Tiger programs
 * Copyright (C) 2017  Casper Freksen

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. *)

structure GenTig:
          sig
              val generate: unit -> Absyn.exp
          end =
struct

structure A = Absyn
structure S = Symbol
structure Ty = Types
structure U = GenTigUtil

exception GenTigError
fun err msg = (print ("GenTigError: " ^ msg ^ "\n"); raise GenTigError)
exception NotImplemented
fun TODO msg = (print ("GT TODO!\nAt " ^ msg ^ "\n"); raise NotImplemented)

(* Dummy value *)
val pos = 99

datatype prec = pexp
              | pdiv
              | pmul
              | pmin
              | pplu

datatype value = Int of int
               | StringVal of string * int * int (* string, size, ord *)
datatype venventry = VarEntry of value
                   | FunEntry of {params : value list, result : value}
type tenventry = Ty.ty          (* TODO *)

type venv = (S.symbol * venventry) list
type tenv = (S.symbol * tenventry) list

val mt_venv = []
val mt_tenv = []

(* TODO: Add more *)
val string_atoms = ["Hi", "Hello", "Car", "Train", "Cat", "Dog", "Mouse",
                    "Bird", "Big", "Best", "USA", "DK", "Dovs", "Lexer",
                    "Semant", "Parser", "Irgen", "X86gen", "Nice", "Mom", "Dad",
                    "Sis", "Bro", "Fun",
                    "Bob", "Jim", "John", "Jay", "Matt", "Rob", "Joe", "Roy", "Lee", "Ray",
                    "Sue", "Alice", "Mary", "Barb", "Amy", "Pam", "Ann", "Kate", "Jane"]
val string_chars = map str (explode "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 !.,:;?")

fun generate () =
  let val rand_seed = let val bigtime = (Time.toMicroseconds o Time.now) ()
                      in Int.fromLarge (bigtime mod
                                        (case (LargeInt.fromString "107374183")
                                          of SOME i => i
                                           | NONE => err "seed"))
                         handle Overflow => (print "Overflow in seed\n";
                                             5)
                      end
      val rand = Random.rand (1, rand_seed)
      fun rnat limit = if limit > 0
                       then Random.randRange (0, limit) rand
                       else 0
      fun rint limit = Random.randRange (~limit, limit) rand
      fun choice percentage = let val ticket = (rnat 100)
                              in ticket < percentage
                              end

      fun pick elems =
        let val len = List.length elems
            val idx = rnat (len - 1)
        in List.nth (elems, idx)
        end

      fun pick_safe [] = NONE
        | pick_safe elems = SOME (pick elems)

      fun mk_name tries : S.symbol =
        let fun aux 0 = ""
              | aux n = pick string_atoms ^ aux (n - 1)
        in (S.symbol o aux) (tries  + 1 + rnat 2)
        end

      fun mk_name_unique used tries : S.symbol =
        let val pick = mk_name tries
        in if List.exists (fn e => e = pick) used
           then mk_name_unique used (tries + 1)
           else pick
        end

      fun factors2i fs =
        let val prob = 100 div ((List.length fs) + 1) + 1
            fun aux [] = 1
              | aux (f :: fs') = (if choice prob
                                  then f
                                  else 1) * (aux fs')
        in aux fs
        end


      datatype expty = Plu | Min | Mul | Div | Exp | Lit | Let |
                       Var | String | Debug
      fun b2s Plu = "+"
        | b2s Min = "-"
        | b2s Mul = "*"
        | b2s Div = "/"
        | b2s Exp = "^"
        | b2s Lit = "lit"
        | b2s _ = TODO "b2s"
      val exptys = [Plu, Min, Mul, Div, Lit, Exp, Let, Var, String]
      datatype string_exp_ty = SLit | SDebug
      val string_exp_tys = [SLit]
      fun gen_int (venv : venv) tenv (p : prec) (depth : int) (i : int) =
        let fun gi p i = gen_int venv tenv p (depth - 1) i
            fun solve target value absyn =
              let val other = target - value
                  val main = A.OpExp {left = absyn,
                                      oper = A.PlusOp,
                                      right = gi pplu other,
                                      pos = pos}
              in if target = value
                 then absyn
                 else case p
                       of pplu => main
                        | _ => A.SeqExp [(main, pos)]
              end
            val expty = if depth > 0
                        then case pick exptys
                              of Lit => pick exptys (* Lit is boring *)
                               | other => other
                        else Lit
        in case expty
            of Lit =>
               if i >= 0
               then A.IntExp i
               else let val main = A.OpExp {left = A.IntExp 0,
                                            oper = A.MinusOp,
                                            right = A.IntExp (~i),
                                            pos = pos}
                    in case p
                        of pplu => main
                         | _ => A.SeqExp [(main, pos)]
                    end
             | Plu =>
               let  (* + *)
                   val split = rnat i
                   val main = A.OpExp {left = gi pplu split,
                                       oper = A.PlusOp,
                                       right = gi pplu (i - split),
                                       pos = pos}
               in case p
                   of pplu => main
                    | _ => A.SeqExp [(main, pos)]
               end
             | Min =>
               let  (* - *)
                   val diff = rint (Int.abs i)
                   val main = A.OpExp {left = gi pplu (i + diff),
                                       oper = A.MinusOp,
                                       right = gi pmin diff,
                                       pos = pos}
               in case p
                   of pplu => main
                    | _ => A.SeqExp [(main, pos)]
               end
             | Mul =>
               let  (* * *)
                   val ff = (factors2i o U.factor) i
                   val main = if choice 50
                              then A.OpExp {left = gi pmul ff,
                                            oper = A.TimesOp,
                                            right = gi pmul (i div ff),
                                            pos = pos}
                              else  A.OpExp {left = gi pmul (i div ff),
                                             oper = A.TimesOp,
                                             right = gi pmul ff,
                                             pos = pos}
               in case p
                   of (pplu | pmin | pmul) => main
                    | _ => A.SeqExp [(main, pos)]
               end
             | Exp =>
               let  (* ^ *)
                   val base = (rnat 4) + 1
                   val expn = rnat 4
                   val value = U.pow base expn
                   val exp_exp = A.OpExp {left = gi pexp base,
                                          oper = A.ExponentOp,
                                          right = gi pexp expn,
                                          pos = pos}
               in solve i value exp_exp
               end
             | Div =>
               (let  (* / *)
                   val fact = 1 + rnat (depth + 4)
                   val fact' = i * fact + (if i >= 0
                                           then (rnat (fact - 1))
                                           else 0 (* TODO figure out
                                                   * how include this case *))
                   val main = A.OpExp {left = gi pdiv fact',
                                       oper = A.DivideOp,
                                       right = gi pdiv fact,
                                       pos = pos}
               in (case p
                    of (pplu | pmin | pmul) => main
                     | _ => A.SeqExp [(main, pos)])
               end
                handle Overflow => gi p i)
             | Let =>
               let  (* let *)
                   val (venv', tenv', decls) = gen_decls venv tenv depth
                   val body = gen_int venv' tenv' pplu (depth - 1) i
               in A.LetExp {decls = decls,
                            body = body,
                            pos = pos}
               end
             | Var =>
               (case find_int venv tenv
                 of SOME (absyn, value) =>
                    solve i value absyn
                  | NONE => gi p i)
             | String =>
               let val len = rnat (Int.min (abs i, 20))
                   val (str, _, ordd) = gen_string venv tenv len (depth - 1)
                   val (absyn, v) = if choice 50
                                    then (A.CallExp {func = S.symbol "size",
                                                     args = [(str, pos)],
                                                     pos = pos}, len)
                                    else (A.CallExp {func = S.symbol "ord",
                                                     args = [(str, pos)],
                                                     pos = pos}, ordd)
               in solve i v absyn
               end
             | _ => TODO "unknown expty"
        end
      and gen_string (venv : venv) tenv len depth =
          let val string_exp_ty = if depth > 0
                                  then pick string_exp_tys
                                  else SLit
              fun fill_string 0 = ""
                | fill_string length =
                  pick string_chars ^ fill_string (length - 1)
          in case string_exp_ty
              of SLit =>
                 let fun aux 0 = ""
                       | aux budget =
                         let val chosen = pick string_atoms
                             val cost = String.size chosen
                         in if cost > budget
                            then fill_string budget
                            else chosen ^ aux (budget - cost)
                         end
                     val result = aux len
                     val ordd = U.ord result
                 in (A.StringExp (result, pos), len, ordd)
                 end
               | _ => TODO "Unkown string_exp_ty"
          end
      and find_int (venv : venv) tenv =
            (* TODO: Is filtration needed? It might even be dangerous *)
            case pick_safe (map #1 venv)
             of NONE => NONE
              | SOME key =>
                let val entry = case U.alook key venv
                                 of SOME e => e
                                  | NONE => err ("Could not find key " ^
                                                 S.name key ^
                                                 " in venv.")
                in case entry
                    of VarEntry (Int i) =>
                       let val varexp = A.VarExp (A.SimpleVar (key, pos))
                       in SOME (varexp, i)
                       end
                     | VarEntry _ => TODO "find var other"
                     | FunEntry {params, result} =>
                       let val args = map (fn param =>
                                              case param
                                               of Int i =>
                                                  (gen_int venv tenv pplu 2 i, pos)
                                                | _ => TODO "callexp idk param")
                                          params
                           val result_i = case result
                                           of Int i => i
                                            | _ => TODO "callexp result ty"
                       in SOME (A.CallExp {func = key,
                                           args = args,
                                           pos = pos},
                                result_i)
                       end
                end
      and gen_decls (venv_init : venv) tenv_init (depth : int) =
          let fun mk_var name value venv tenv =
                let val init = gen_int venv tenv pplu (depth - 1) value
                    val dec = A.VarDec {name = name,
                                        escape = ref true,
                                        typ = NONE, (* TODO *)
                                        init = init,
                                        pos = pos}
                    val entry = VarEntry (Int value)
                in (dec, name, entry)
                end

              fun mk_param venv tenv used =
                let val name = mk_name_unique used 0
                    val value = rnat 100
                    val typ = (S.symbol "int", pos)
                    val venvdec = (name, VarEntry (Int value))
                    val fielddata = {name = name,
                                     escape = ref true,
                                     typ = typ,
                                     pos = pos}
                in (name, venvdec, fielddata, Int value)
                end

              fun mk_params venv tenv n fun_name =
                let fun aux 0 _ = []
                      | aux n (used : S.symbol list) =
                        let val (nxt_name, nv, nf, v) = mk_param venv tenv used
                        in (nv, nf, v) :: aux (n - 1) (nxt_name :: used)
                        end
                in aux n [fun_name]
                end

              fun mk_fun name value venv tenv =
                let val params = mk_params venv tenv (rnat 4) name
                    val resultty = S.symbol "int" (* TODO: Use tenv *)
                    val venv_extend = map #1 params
                    val venv' = venv_extend @ venv
                    val body = gen_int venv' tenv pplu (depth - 1) value
                    val dec = A.FunctionDec [{name = name,
                                              params = map #2 params,
                                              result = SOME (resultty, pos),
                                              body = body,
                                              pos = pos}]
                    val entry = FunEntry {params = map #3 params,
                                          result = Int value}
                in (dec, name, entry)
                end

              fun mk_dec venv tenv =
                let val name = mk_name 0 (* TODO: This needs to be
                                            unique for sequential
                                            functions *)
                    val value = rnat 100
                in (if choice 50
                    then mk_var
                    else mk_fun) name value venv tenv
                end
              fun aux venv tenv 0 = (venv, tenv, [])
                | aux venv tenv ndec =
                  let val (dec, name, entry) = mk_dec venv tenv
                      (* val entry = VarEntry (Int value) (* TODO: Wrong *) *)
                      val venv' = (name, entry) :: venv
                      val (venv'', tenv'', decs) = aux venv' tenv (ndec - 1)
                  in (venv'', tenv'', dec :: decs)
                  end
          in aux venv_init tenv_init (rnat 5)
          end
      val std_gen = gen_int mt_venv mt_tenv pplu 4
  in let val iexp = std_gen (2 * 2 * 3 * 3 * 5)
                    handle _ => (print "An Exception Ocurred\n"; A.NilExp)
     in iexp
     end
  end

end (* GenTig *)
